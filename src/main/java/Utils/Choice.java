package Utils;

public enum Choice
{
    // 0
    PRINT_OPTIONS(0),
    // 1
    LOAD_REG_NUMS(1),
    // 2
    PROCESS_TOLL_EVENTS(2),
    // 3
    WRITE_TOLL_EVENTS(3),
    // 4
    DELETE_TOLL_EVENTS(4),
    // 5
    QUIT(5),
    // -1
    DEFAULT(-1);

    Choice choice;

    private int intValue = 0;

    private Choice(int intValue)
    {
        this.intValue = intValue;
    }

    public int getValue()
    {
        return intValue;
    }

    public void setValue(int intValue)
    {
        this.intValue = intValue;
    }

    public Choice getChoice(int intValue)
    {
        switch (intValue)
        {
            case 0:
                return choice.PRINT_OPTIONS;

            case 1:
                return choice.LOAD_REG_NUMS;

            case 2:
                return choice.PROCESS_TOLL_EVENTS;

            case 3:
                return choice.WRITE_TOLL_EVENTS;
                
            case 4:
                return choice.DELETE_TOLL_EVENTS;

            case 5:
                return choice.QUIT;

            default:
                return choice.DEFAULT;

        }

    }

}
