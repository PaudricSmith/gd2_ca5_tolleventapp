package DAOs;

import Exceptions.DAOException;
import java.util.Set;

public interface IVehiclesDAOInterface
{

    // Get all regNumbers from Vehicles database
    public Set<String> findAllRegNumbers() throws DAOException;

}
