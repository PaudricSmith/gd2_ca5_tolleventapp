package DAOs;

import DTOs.TollEvent;
import Exceptions.DAOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public interface ITollEventDAOInterface
{

    // Get all Toll Event details
    public List<TollEvent> findAllTollEvents() throws DAOException;

    // Get all Toll Event details returned as type Map<registration, List<TollEvent>>
    public Map<String, List<TollEvent>> findAllAsMapRegToListOfAllTollEvents() throws DAOException;

    // Get toll event by vehicle reg
    public List<TollEvent> findTollEventByReg(String reg) throws DAOException;

    // Get all Toll Event details that happened since a specified date-time
    public List<TollEvent> findAllTollEventsSince(Timestamp ts) throws DAOException;

    // Get all Toll Event details that happened between 2 date-times
    public List<TollEvent> findAllTollEventsBetween(Timestamp firstTs, Timestamp lastTs) throws DAOException;

    // Get all Toll Event details that happened between 2 date-times
    public List<String> findAllRegistrationsPassed() throws DAOException;

    // Write all Toll Event details to tollEvent database table
    public void writeAllValidTollEvents(Map<String, List<TollEvent>> mapTollEvents) throws DAOException;

    // Delete all Toll Event details from database
    public void deleteAllTollEvents() throws DAOException;
    
}
