package DAOs;

import DTOs.TollEvent;
import Daos.MySqlDAO;
import Exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySqlTollEventDAO extends MySqlDAO implements ITollEventDAOInterface
{

    // Get all Toll Event details 
    @Override
    public List<TollEvent> findAllTollEvents() throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList<>();

        try
        {
            con = this.getConnection();

            // SQL query
            String query = "SELECT * FROM toll_event";
            // make prepared statement
            ps = con.prepareStatement(query);
            // make result set
            rs = ps.executeQuery();

            while (rs.next())
            {
                // set variables
                String registration = rs.getString("registration");
                long imageId = rs.getLong("image_id");
                Timestamp ts = rs.getTimestamp("timestamp");
                Instant instant = ts.toInstant();

                // make a new TollEvent
                TollEvent t = new TollEvent(registration, imageId, instant);

                // Add each TollEvent in the database to this List
                tollEvents.add(t);

            }

        } catch (SQLException e)
        {
            throw new DAOException("findAllTollEvents()" + e.getMessage());
        } finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("findAllTollEvents" + e.getMessage());
            }
        }
        return tollEvents;
    }

    // Get all Toll Event details 
    @Override
    public Map<String, List<TollEvent>> findAllAsMapRegToListOfAllTollEvents() throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String, List<TollEvent>> map = new HashMap<>();
        List<TollEvent> tollEvents;

        try
        {
            con = this.getConnection();

            // SQL query
            String query = "SELECT * FROM toll_event";
            // make prepared statement
            ps = con.prepareStatement(query);
            // make result set
            rs = ps.executeQuery();

            while (rs.next())
            {
                // set variables
                tollEvents = new ArrayList<>();
                String registration = rs.getString("registration");
                long imageId = rs.getLong("image_id");
                Timestamp ts = rs.getTimestamp("timestamp");
                Instant instant = ts.toInstant();

                // make a new TollEvent
                TollEvent t = new TollEvent(registration, imageId, instant);

                // Add to the map
                if (map.containsKey(registration))
                {
                    map.get(registration).add(t);
                } else
                {
                    tollEvents.add(t);
                    map.put(registration, tollEvents);
                }

            }

        } catch (SQLException e)
        {
            throw new DAOException("findAllAsMapRegToListOfAllTollEvents()" + e.getMessage());
        } finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("findAllAsMapRegToListOfAllTollEvents()" + e.getMessage());
            }
        }
        return map;
    }

    // Get toll event by vehicle reg
    @Override
    public List<TollEvent> findTollEventByReg(String reg) throws DAOException
    {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList<>();

        try
        {
            con = this.getConnection();
            String query = "SELECT * FROM toll_event WHERE registration = ?";
            // make prepared statement
            ps = con.prepareStatement(query);
            // set the variables to the '?'
            ps.setString(1, reg);
            rs = ps.executeQuery();

            while (rs.next())
            {
                // set variables
                String registration = rs.getString("registration");
                long imageId = rs.getLong("image_id");
                Timestamp ts = rs.getTimestamp("timestamp");
                Instant instant = ts.toInstant();

                // make a new TollEvent
                TollEvent tollEvent = new TollEvent(registration, imageId, instant);

                // Add each TollEvent in the database to this List
                tollEvents.add(tollEvent);

            }

        } catch (SQLException e)
        {
            throw new DAOException("findTollEventByReg()" + e.getMessage());
        } finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("findTollEventByReg()" + e.getMessage());
            }
        }

        return tollEvents;
    }

    // Get all Toll Event details that happened since a specified date-time
    @Override
    public List<TollEvent> findAllTollEventsSince(Timestamp ts) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList<>();

        try
        {
            con = this.getConnection();

            // SQL query
            String query = "SELECT * FROM toll_event WHERE timestamp > ?";
            // make prepared statement
            ps = con.prepareStatement(query);
            // set the variables to the '?'
            ps.setTimestamp(1, ts);
            // make result set
            rs = ps.executeQuery();

            while (rs.next())
            {
                // set variables
                String registration = rs.getString("registration");
                long imageId = rs.getLong("image_id");
                ts = rs.getTimestamp("timestamp");
                Instant instant = ts.toInstant();

                // make a new TollEvent
                TollEvent t = new TollEvent(registration, imageId, instant);

                // Add each TollEvent in the database to this List
                tollEvents.add(t);

            }

        } catch (SQLException e)
        {
            throw new DAOException("findAllTollEventsSince()" + e.getMessage());
        } finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("findAllTollEventsSince" + e.getMessage());
            }
        }
        return tollEvents;
    }

    // Get all Toll Event details that happened between 2 date-times
    @Override
    public List<TollEvent> findAllTollEventsBetween(Timestamp firstTs, Timestamp lastTs) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList<>();

        try
        {
            con = this.getConnection();

            // SQL query
            String query = "SELECT * FROM toll_event WHERE timestamp BETWEEN ? AND ?";
            // make prepared statement
            ps = con.prepareStatement(query);
            // set the variables to the '?'
            ps.setTimestamp(1, firstTs);
            ps.setTimestamp(2, lastTs);
            // make result set
            rs = ps.executeQuery();

            while (rs.next())
            {
                // set variables
                String registration = rs.getString("registration");
                long imageId = rs.getLong("image_id");
                Timestamp ts = rs.getTimestamp("timestamp");
                Instant instant = ts.toInstant();

                // make a new TollEvent
                TollEvent t = new TollEvent(registration, imageId, instant);

                // Add each TollEvent in the database to this List
                tollEvents.add(t);

            }

        } catch (SQLException e)
        {
            throw new DAOException("findAllTollEventsBetween()" + e.getMessage());
        } finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("findAllTollEventsBetween()" + e.getMessage());
            }
        }
        return tollEvents;
    }

    // Get all Toll Event details that happened between 2 date-times
    @Override
    public List<String> findAllRegistrationsPassed() throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> registrations = new ArrayList<>();

        try
        {
            con = this.getConnection();

            // SQL query
            String query = "SELECT DISTINCT registration FROM toll_event ORDER BY registration";
            // make prepared statement
            ps = con.prepareStatement(query);
            // make result set
            rs = ps.executeQuery();

            while (rs.next())
            {
                // set variables
                String registration = rs.getString("registration");

                // Add each registration from the query to registrations List
                registrations.add(registration);

            }

        } catch (SQLException e)
        {
            throw new DAOException("findAllRegistrationsPassed()" + e.getMessage());
        } finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("findAllRegistrationsPassed()" + e.getMessage());
            }
        }
        return registrations;
    }

    @Override
    public void writeAllValidTollEvents(Map<String, List<TollEvent>> mapTollEvents) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        String registration = "";
        long imageId = 0;
        Instant instant = null;
        Timestamp tsFromInstant = new Timestamp(00 - 00 - 00);

        try
        {
            con = this.getConnection();

            for (Map.Entry<String, List<TollEvent>> entry : mapTollEvents.entrySet())
            {

                List<TollEvent> value = entry.getValue();

                for (TollEvent tollEvent : value)
                {
                    registration = tollEvent.getRegistration();
                    imageId = tollEvent.getImageId();
                    instant = tollEvent.getTimestamp();
                    tsFromInstant = Timestamp.from(instant);
                }

                // SQL query
                String query = "INSERT INTO toll_event (registration, image_id, timestamp) VALUES (?,?,?);";
                // make prepared statement
                ps = con.prepareStatement(query);
                // set the variables to the '?'
                ps.setString(1, registration);
                ps.setLong(2, imageId);
                ps.setTimestamp(3, tsFromInstant);
                ps.executeUpdate();

            }
            System.out.println("\nBatch Job completed!");

        } catch (SQLException e)
        {
            throw new DAOException("writeAllTollEvents()" + e.getMessage());
        } finally
        {
            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("writeAllTollEvents()" + e.getMessage());
            }
        }

    }

    // Delete all Toll Event details from database
    public void deleteAllTollEvents() throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;

        try
        {
            con = this.getConnection();

            // SQL query
            String query = "DELETE FROM toll_event";
            // make prepared statement
            ps = con.prepareStatement(query);
            ps.executeUpdate();

        } catch (SQLException e)
        {
            throw new DAOException("deleteAllTollEvents()" + e.getMessage());
        } finally
        {
            try
            {
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("delteAllTollEvents" + e.getMessage());
            }
        }
    }

}
