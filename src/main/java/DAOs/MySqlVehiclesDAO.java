package DAOs;

import Daos.MySqlDAO;
import Exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class MySqlVehiclesDAO extends MySqlDAO implements IVehiclesDAOInterface
{

    // Get all vehicle reg numbers from Vehicles database
    @Override
    public Set<String> findAllRegNumbers() throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Set<String> regNumbers = new TreeSet<>();

        try
        {
            con = this.getConnection();

            // SQL query
            String query = "SELECT reg_nums FROM vehicles";
            // make prepared statement
            ps = con.prepareStatement(query);
            // make result set
            rs = ps.executeQuery();

            while (rs.next())
            {
                // set variable
                String regNumber = rs.getString("reg_nums");

                // Add each regNumber in the database to this List
                regNumbers.add(regNumber);

            }

        } catch (SQLException e)
        {
            throw new DAOException("findAllRegNumbers()" + e.getMessage());
        } finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (ps != null)
                {
                    ps.close();
                }
                if (con != null)
                {
                    freeConnection(con);
                }
            } catch (SQLException e)
            {
                throw new DAOException("findAllRegNumbers()" + e.getMessage());
            }
        }
        return regNumbers;
    }

}
