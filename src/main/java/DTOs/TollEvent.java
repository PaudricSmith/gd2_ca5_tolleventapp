package DTOs;

import java.time.Instant;

public class TollEvent
{

    private String registration;
    private long imageId;
    private Instant timestamp;

    public TollEvent(String registration, long imageId, Instant timestamp)
    {
        this.registration = registration;
        this.imageId = imageId;
        this.timestamp = timestamp;
    }

    public String getRegistration()
    {
        return registration;
    }

    public long getImageId()
    {
        return imageId;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }

    @Override
    public String toString()
    {
        return "TollEvent{" + "reg = " + registration + ", imageId = " + imageId + ", timestamp = " + timestamp + '}';
    }

}
