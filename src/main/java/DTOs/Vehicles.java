package DTOs;

public class Vehicles
{

    private String regNumber;

    public Vehicles(String regNumber)
    {
        this.regNumber = regNumber;
    }

    public String getRegNumber()
    {
        return regNumber;
    }

    @Override
    public String toString()
    {
        return "Vehicles{" + "regNumber=" + regNumber + '}';
    }

}
