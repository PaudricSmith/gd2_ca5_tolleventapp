package BusinessObjects;

import DAOs.ITollEventDAOInterface;
import DAOs.IVehiclesDAOInterface;
import DAOs.MySqlTollEventDAO;
import DAOs.MySqlVehiclesDAO;
import DTOs.TollEvent;
import Exceptions.DAOException;
import Utils.Choice;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainApp
{

    // App variables
    private static Scanner SCANNER = new Scanner(System.in);
    private static Set<String> lookUpTable = new TreeSet<>();
    private static Map<String, List<TollEvent>> validTollEvents = new HashMap<>();
    private static Map<String, List<TollEvent>> initialTollEvents = new HashMap<>();
    private static List<String> invalidRegistrations = new ArrayList<>();
    private static ITollEventDAOInterface tollEventDAO = new MySqlTollEventDAO();

    public static void main(String[] args)
    {
        MainApp mainApp = new MainApp();
        mainApp.start();
    }

    private static void start()
    {
        mainMenu();
    }

    private static void mainMenu()
    {
        boolean quit = false;
        boolean isNumber = false;
        Choice choice = Choice.DEFAULT;

        printOptions();

        do
        {

            // Prevent user from entering anything other than an integer for the choice
            if (SCANNER.hasNextInt())
            {
                choice = choice.getChoice(SCANNER.nextInt());
                SCANNER.nextLine();
                isNumber = true;
            } else
            {
                choice = choice.DEFAULT;
                SCANNER.next();
            }

            switch (choice)
            {
                case PRINT_OPTIONS:
                    printOptions();
                    break;
                case LOAD_REG_NUMS:
                    loadRegNumbers();
                    break;
                case PROCESS_TOLL_EVENTS:
                    processTollEvents();
                    break;
                case WRITE_TOLL_EVENTS:
                    writeTollEvents(validTollEvents);
                    break;
                case DELETE_TOLL_EVENTS:
                    deleteAllTollEvents();
                    break;
                case QUIT:
                    System.out.println("\nQuiting...");
                    quit = true;
                    break;
                case DEFAULT:
                default:
                    System.out.println("\nOnly enter an integer from 0 - 4");
                    System.out.println("Enter an option:");
                    break;
            }
        } while (choice.ordinal() < 0 || choice.ordinal() > 6 || isNumber == false || quit == false);

    }

    private static void printOptions()
    {
        System.out.println("\nPlease enter one of the following:");
        System.out.println("0 - To print options: ");
        System.out.println("1 - To load registration numbers: ");
        System.out.println("2 - To process toll events: ");
        System.out.println("3 - To write toll events: ");
        System.out.println("4 - To delete all toll events: ");
        System.out.println("5 - To Quit:\n");

    }

    private static void loadRegNumbers()
    {
        IVehiclesDAOInterface vehicleDao = new MySqlVehiclesDAO();
        try
        {
            lookUpTable = vehicleDao.findAllRegNumbers();
            if (lookUpTable.isEmpty())
            {
                System.out.println("There are no vehicle registration numbers!");
            } else
            {
                System.out.println("Registration numbers loaded successfully!");
            }
            printOptions();

        } catch (DAOException e)
        {
            System.out.println(e.getMessage());
        }

    }

    private static void processTollEvents()
    {

        System.out.println("Processing toll events ...");

        String tollEventsFile = "src/main/java/Utils/tollEvents.csv";

        List<TollEvent> tollEvents;

        try (Scanner in = new Scanner(new FileReader(tollEventsFile)))
        {

            in.useDelimiter(";");
            while (in.hasNextLine())
            {
                // Set variables
                tollEvents = new ArrayList<>();
                String registration = in.next();
                in.skip(in.delimiter());

                // Pattern takes 1 to 3 digits at start, then the first letter has to be one of [CDGKLMORSTW]
                // and the second has to be one of [DEHKLMNOSWXY] if it has a 2 letter abreviation.
                // Then to finish off the licence plate it can take from 1 to 6 digits.
                Pattern irishPattern = Pattern.compile("^[0-9]{1,3}([CDGKLMORSTW]{1}([DEHKLMNOSWXY]{1})?)[0-9]{1,6}$");
                Matcher matcher = irishPattern.matcher(registration);

                long imageId = in.nextLong();
                in.skip(in.delimiter());
                String rawStrTimestamp = in.nextLine();
                String strTimestamp = rawStrTimestamp.replaceAll("[T,Z]", " ");
                Timestamp timestamp = Timestamp.valueOf(strTimestamp);
                Instant instant = timestamp.toInstant();

                // make a new TollEvent
                TollEvent t = new TollEvent(registration, imageId, instant);

                // If the registration from the file has the right irish registration pattern
                if (matcher.matches())
                {
                    System.out.println("Reg matches!");

                    // Check if the registration is in the lookUpTable of registered Irish plates
                    if (lookUpTable.contains(registration))
                    {
                        // Add to the mapTollEvents HashMap
                        if (validTollEvents.containsKey(registration))
                        {
                            validTollEvents.get(registration).add(t);
                        } else
                        {
                            tollEvents.add(t);
                            validTollEvents.put(registration, tollEvents);
                        }

                    } else // If the registration isn't a registered Irish plate 
                    {
                        System.out.println("Reg not registered!");
                        invalidRegistrations.add(registration);
                    }

                } else // If the registration isn't an Irish registration pattern 
                {
                    System.out.println("Reg doesn't match!");
                    invalidRegistrations.add(registration);
                }

            }
            printOptions();

        } catch (IOException ioe)
        {
            System.out.println("Unable to locate file. Program will exit.\n"
                    + ioe.toString());
            System.exit(0);
        }

    }

    private static void writeTollEvents(Map<String, List<TollEvent>> validTollEvents)
    {
        // At 4am every day batch job to database

        System.err.println("ADVISERY!: EXECUTE BATCH JOB WHEN SYSTEM IS NOT BUSY (4am)");
        System.err.println("Are you sure you want to execute batch job?");
        System.out.println("Y / N");

        boolean isValid = false;
        String userInput = "";

        do
        {
            userInput = SCANNER.nextLine();

            switch (userInput)
            {
                case "Y":
                case "y":
                try
                    {
                        tollEventDAO.writeAllValidTollEvents(validTollEvents);
                        System.out.println("Returning back to Main Menu.");
                } catch (DAOException e)
                {
                    System.out.println(e.getMessage());
                }
                isValid = true;
                break;
                case "N":
                case "n":
                    System.out.println("Returning back to Main Menu.");
                    isValid = true;
                    break;
                default:
                    System.out.println("Enter only 'Y' or 'N' thank you!");
                    isValid = false;
                    break;

            }
        } while (isValid == false);

        printOptions();
    }

    private static void deleteAllTollEvents()
    {

        System.err.println("Are you sure you want to delete all Toll Events?");
        System.out.println("Y / N");

        boolean isValid = false;
        String userInput = "";

        do
        {
            userInput = SCANNER.nextLine();

            switch (userInput)
            {
                case "Y":
                case "y":
                try
                    {
                        if (tollEventDAO.findAllTollEvents().isEmpty())
                        {
                            System.out.println("There are no Toll Events to delete.");
                            System.out.println("Returning to Main Menu.");

                        } else
                        {
                            tollEventDAO.deleteAllTollEvents();
                            System.out.println("All Toll Events deleted!!!");
                        }

                } catch (DAOException e)
                {
                    System.out.println(e.getMessage());
                }
                isValid = true;
                break;
                case "N":
                case "n":
                    System.out.println("Returning back to Main Menu.");
                    isValid = true;
                    break;
                default:
                    System.out.println("Enter only 'Y' or 'N' thank you!");
                    isValid = false;
                    break;

            }
        } while (isValid == false);

        printOptions();
    }

}
