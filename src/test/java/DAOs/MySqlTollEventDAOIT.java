package DAOs;

import DTOs.TollEvent;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Paudric
 */
public class MySqlTollEventDAOIT
{

    private MySqlTollEventDAO instance = new MySqlTollEventDAO();

    public MySqlTollEventDAOIT()
    {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception
    {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception
    {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception
    {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception
    {
    }

    /**
     * Test of findAllTollEvents method, of class MySqlTollEventDAO.
     */
    @org.junit.jupiter.api.Test
    public void testFindAllTollEvents() throws Exception
    {
        System.out.println("findAllTollEvents");
        instance = new MySqlTollEventDAO();
        List<TollEvent> expResult = null;
        List<TollEvent> result = instance.findAllTollEvents();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findAllAsMapRegToListOfAllTollEvents method, of class MySqlTollEventDAO.
     */
    @org.junit.jupiter.api.Test
    public void testFindAllAsMapRegToListOfAllTollEvents() throws Exception
    {
        System.out.println("findAllAsMapRegToListOfAllTollEvents");
        instance = new MySqlTollEventDAO();
        Map<String, List<TollEvent>> expResult = null;
        Map<String, List<TollEvent>> result = instance.findAllAsMapRegToListOfAllTollEvents();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findTollEventByReg method, of class MySqlTollEventDAO.
     */
    @org.junit.jupiter.api.Test
    public void testFindTollEventByReg() throws Exception
    {
        System.out.println("findTollEventByReg");
        String reg = "182D3456";
        String strTimestamp = "2020-02-14 10:15:31.800";
        Timestamp timestamp = Timestamp.valueOf(strTimestamp);
        Instant expInstant = timestamp.toInstant();
        
        List<TollEvent> result = instance.findTollEventByReg(reg);
        String resultReg = result.get(0).getRegistration();
        long resultId = result.get(0).getImageId();
        Instant resultInstant = result.get(0).getTimestamp();
        
        assertEquals(reg, resultReg);
        assertEquals(30403, resultId);
        assertEquals(expInstant, resultInstant);

    }

    /**
     * Test of findAllTollEventsSince method, of class MySqlTollEventDAO.
     */
    @org.junit.jupiter.api.Test
    public void testFindAllTollEventsSince() throws Exception
    {
        System.out.println("findAllTollEventsSince");
        Timestamp ts = null;
        instance = new MySqlTollEventDAO();
        List<TollEvent> expResult = null;
        List<TollEvent> result = instance.findAllTollEventsSince(ts);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findAllTollEventsBetween method, of class MySqlTollEventDAO.
     */
    @org.junit.jupiter.api.Test
    public void testFindAllTollEventsBetween() throws Exception
    {
        System.out.println("findAllTollEventsBetween");
        Timestamp firstTs = null;
        Timestamp lastTs = null;
        instance = new MySqlTollEventDAO();
        List<TollEvent> expResult = null;
        List<TollEvent> result = instance.findAllTollEventsBetween(firstTs, lastTs);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findAllRegistrationsPassed method, of class MySqlTollEventDAO.
     */
    @org.junit.jupiter.api.Test
    public void testFindAllRegistrationsPassed() throws Exception
    {
        System.out.println("findAllRegistrationsPassed");
        instance = new MySqlTollEventDAO();
        List<String> expResult = null;
        List<String> result = instance.findAllRegistrationsPassed();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
